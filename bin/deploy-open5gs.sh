#!/bin/bash
set -ex
BINDIR=`dirname $0`
source $BINDIR/common.sh

sudo sysctl -w net.ipv4.ip_forward=1
sudo iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o ogstun -j MASQUERADE

if [ -f $SRCDIR/open5gs-setup-complete ]; then
    echo "setup already ran; not running again"
    exit 0
fi

sudo apt update
sudo apt install -y software-properties-common
sudo add-apt-repository -y ppa:wireshark-dev/stable
echo "wireshark-common wireshark-common/install-setuid boolean false" | sudo debconf-set-selections
sudo apt update
sudo apt-get install gnupg
curl -fsSL https://pgp.mongodb.com/server-6.0.asc | \
    sudo gpg -o /usr/share/keyrings/mongodb-server-6.0.gpg --dearmor
echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-6.0.gpg ] https://repo.mongodb.org/apt/ubuntu $(lsb_release -cs)/mongodb-org/6.0 multiverse" | \
    sudo tee /etc/apt/sources.list.d/mongodb-org-6.0.list
sudo apt update
sudo apt install -y \
    mongodb-org \
    mongodb-mongosh \
    iperf3 \
    tshark \
    wireshark

cd $SRCDIR

sudo apt install -y python3-pip python3-setuptools python3-wheel ninja-build build-essential flex bison git cmake libsctp-dev libgnutls28-dev libgcrypt-dev libssl-dev libidn11-dev libmongoc-dev libbson-dev libyaml-dev libnghttp2-dev libmicrohttpd-dev libcurl4-gnutls-dev libnghttp2-dev libtins-dev libtalloc-dev meson
sudo git clone https://gitlab.flux.utah.edu/mali3119/open5gs.git

cd open5gs
sudo git config --global --add safe.directory /var/tmp/open5gs
#sudo git checkout v2.7.0 
sudo meson build --prefix=`pwd`/install
sudo ninja -C build
cd build
sudo ninja install
cd ../


sudo systemctl start mongod
sudo systemctl enable mongod

sudo mkdir /etc/open5gs/
sudo mkdir /var/log/open5gs

sudo cp -r /local/repository/etc/open5gs/* /etc/open5gs/

sudo touch /var/log/open5gs/mme.log
sudo touch /var/log/open5gs/sgwc.log
sudo touch /var/log/open5gs/smf.log
sudo touch /var/log/open5gs/amf.log
sudo touch /var/log/open5gs/sgwu.log
sudo touch /var/log/open5gs/upf.log
sudo touch /var/log/open5gs/hss.log
sudo touch /var/log/open5gs/pcrf.log
sudo touch /var/log/open5gs/nrf.log
sudo touch /var/log/open5gs/ausf.log
sudo touch /var/log/open5gs/udm.log
sudo touch /var/log/open5gs/pcf.log
sudo touch /var/log/open5gs/nssf.log
sudo touch /var/log/open5gs/bsf.log
sudo touch /var/log/open5gs/udr.log
sudo touch /var/log/open5gs/scp.log
sudo touch /var/log/open5gs/h-scp.log
sudo touch /var/log/open5gs/h-nrf.log
sudo touch /var/log/open5gs/sepp.log


#sudo systemctl restart open5gs-mmed
#sudo systemctl restart open5gs-sgwcd
#sudo systemctl restart open5gs-smfd
#sudo systemctl restart open5gs-amfd
#sudo systemctl restart open5gs-sgwud
#sudo systemctl restart open5gs-upfd
#sudo systemctl restart open5gs-hssd
#sudo systemctl restart open5gs-pcrfd
#sudo systemctl restart open5gs-nrfd
#sudo systemctl restart open5gs-ausfd
#sudo systemctl restart open5gs-udmd
#sudo systemctl restart open5gs-pcfd
#sudo systemctl restart open5gs-nssfd
#sudo systemctl restart open5gs-bsfd
#sudo systemctl restart open5gs-udrd

# Clone packet rusher
cd ~
git clone https://gitlab.flux.utah.edu/mali3119/packetrusher.git

cd $SRCDIR
wget https://raw.githubusercontent.com/open5gs/open5gs/main/misc/db/open5gs-dbctl
chmod +x open5gs-dbctl
./open5gs-dbctl add_ue_with_apn 999700123456789 00112233445566778899aabbccddeeff 63BFA50EE6523365FF14C1F45F88737D srsapn  # IMSI,K,OPC
./open5gs-dbctl type 999700123456789 1  # APN type IPV4
touch $SRCDIR/open5gs-setup-complete
