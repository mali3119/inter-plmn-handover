#!/bin/sh

cd /var/tmp/open5gs

sudo meson build --prefix=`pwd`/install
sudo ninja -C build
cd build
sudo ninja install


#Kill NFs

sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/amf.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/h-amf.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/smf.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/ausf.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/h-scp.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/udm.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/udr.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/sepp1.yaml' | awk '{print $2}')

sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/nrf.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/scp.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/upf.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/pcf.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/bsf.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/nssf.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/sepp2.yaml' | awk '{print $2}')
sudo kill -SIGINT $(ps aux | grep '/etc/open5gs/h-nrf.yaml' | awk '{print $2}')


# Remove log files

sudo rm /var/log/open5gs/amf.log
sudo rm /var/log/open5gs/h-amf.log
sudo rm /var/log/open5gs/smf.log
sudo rm /var/log/open5gs/ausf.log
sudo rm /var/log/open5gs/h-scp.log
sudo rm /var/log/open5gs/udm.log
sudo rm /var/log/open5gs/udr.log
sudo rm /var/log/open5gs/sepp1.log

sudo sudo rm /var/log/open5gs/nrf.log
sudo sudo rm /var/log/open5gs/scp.log
sudo sudo rm /var/log/open5gs/upf.log
sudo sudo rm /var/log/open5gs/pcf.log
sudo sudo rm /var/log/open5gs/bsf.log
sudo sudo rm /var/log/open5gs/nssf.log
sudo sudo rm /var/log/open5gs/sepp2.log
sudo sudo rm /var/log/open5gs/h-nrf.log


sleep 5

# Start NFs

sudo /var/tmp/open5gs/install/bin/open5gs-amfd -c /etc/open5gs/amf.yaml & 
#sudo gdb --ex run  --args /var/tmp/open5gs/install/bin/open5gs-amfd -c /etc/open5gs/amf.yaml > gdb_output.txt 2>&1 &
sudo /var/tmp/open5gs/install/bin/open5gs-amfd -c /etc/open5gs/h-amf.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-smfd -c /etc/open5gs/smf.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-ausfd -c /etc/open5gs/ausf.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-scpd -c /etc/open5gs/h-scp.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-udmd -c /etc/open5gs/udm.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-udrd -c /etc/open5gs/udr.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-seppd -c /etc/open5gs/sepp1.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-nrfd -c /etc/open5gs/h-nrf.yaml &

sudo /var/tmp/open5gs/install/bin/open5gs-nrfd -c /etc/open5gs/nrf.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-scpd -c /etc/open5gs/scp.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-upfd -c /etc/open5gs/upf.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-pcfd -c /etc/open5gs/pcf.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-bsfd -c /etc/open5gs/bsf.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-nssfd -c /etc/open5gs/nssf.yaml &
sudo /var/tmp/open5gs/install/bin/open5gs-seppd -c /etc/open5gs/sepp2.yaml &

echo "Start core NFs"
sleep 5
# Start
